/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.painfamily.form3.paymentsapi.doctests.config.ApiUrlConfig;
import net.painfamily.form3.paymentsapi.doctests.config.MockResponsesApiUrlConfigs;

public class UpdatePaymentTest extends AbstractDocTest {

    // TODO Replace with a non-mocked config when real implementation is available
    private final ApiUrlConfig urlConfig = MockResponsesApiUrlConfigs.UPDATE_RESPONSE_CONFIG;

    private Response updateResponse;
    private String newAmount;

    @Before
    @Override
    public void setUp() {
        super.setUp();

        // Given a payment resource exists
        JsonPath paymentResource = givenPaymentResource(urlConfig);

        // When the resource is updated
        newAmount = "10.02";
        updateResponse =
        given(this.documentationSpec)
            .contentType(MEDIA_TYPE)
            .body("{\r\n"
                + "    \"data\": {\r\n"
                + "		\"type\": \"" + RESOURCE_TYPE + "\",\r\n"
                + "		\"id\": \"" + paymentResource.get("id") + "\",\r\n"
                + "		\"attributes\": {\r\n"
                + "			\"amount\": \"" + newAmount + "\"\r\n"
                + "    	}\r\n"
                + "	}\r\n"
                + "}")
            .accept(MEDIA_TYPE)
            .filter(document("payments/update",
                requestFields(
                    fieldWithPath("data.type").description("The type of the resource to update: " + RESOURCE_TYPE),
                    fieldWithPath("data.id").description("The ID of the resource to update"),
                    subsectionWithPath("data.attributes").description("The attribute values to set. See the <<resources-payment-retrieve_response_fields, 'retrieve Payment resource' response>> for the available fields.")
                ),
                responseFields(
                    subsectionWithPath("data").description("The <<resources-payment-retrieve, Payment resource object>> representing the updated resource and its new attribute values"),
                    fieldWithPath("data.links.self").description("The URL of the updated resource")
                )
            ))
        .when()
            .patch(paymentResource.getString("data.links.self"));
    }


    @Test
    @Ignore
    public void updatePaymentReturnsUpdatedValue() throws Exception {
        updateResponse
        .then()
            .statusCode(is(200))
            .body("data.attributes.amount", is(newAmount));
    }

    @Test
    @Ignore
    public void updatePaymentResponseMatchesNewPaymentResourceResponse() {
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
        .when()
            .get(updateResponse.<String>path("data.links.self"))
        .then()
            // TODO compare root once not using mocky.io
            .body("data.attributes", equalTo(updateResponse.body().jsonPath().getMap("data.attributes")));
    }

}
