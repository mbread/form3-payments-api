/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.painfamily.form3.paymentsapi.doctests.config.ApiUrlConfig;
import net.painfamily.form3.paymentsapi.doctests.config.MockResponsesApiUrlConfigs;

public class DeletePaymentTest extends AbstractDocTest {

    // TODO Replace with a non-mocked config when real implementation is available
    private final ApiUrlConfig urlConfig = MockResponsesApiUrlConfigs.DELETION_RESPONSE_CONFIG;

    private JsonPath paymentResource;
    private Response deletionResponse;

    @Before
    @Override
    public void setUp() {
        super.setUp();

        // Given a payment resource exists
        paymentResource = givenPaymentResource(urlConfig);

        // When the resource is deleted
        deletionResponse =
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
            .filter(document("payments/delete"))
        .when()
            .delete(paymentResource.getString("data.links.self"));
    }


    @Test
    @Ignore
    public void deletePaymentReturnsEmptyResponse() throws Exception {
        deletionResponse
        .then()
            .statusCode(is(204))
            .content(isEmptyOrNullString());
    }

    @Test
    @Ignore("Can't run this test with mocked endpoints") // TODO
    public void deletePaymentRemovesPaymentResource() {
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
        .when()
            .get(paymentResource.getString("data.links.self"))
        .then()
            .statusCode(is(404));
    }

}
