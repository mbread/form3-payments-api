package net.painfamily.form3.paymentsapi.doctests.config;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * An implementation of {@link ApiUrlConfig} that builds the URLs using
 * "localhost" and a given port number.
 */
public class LocalhostApiUrlConfig implements ApiUrlConfig {

    private static final String HTTP_SCHEME = "http";
    private static final String LOCALHOST = "localhost";

    private int port;

    public LocalhostApiUrlConfig(int port) {
        this.port = port;
    }

    @Override
    public URI getCollectionUri() {
        return buildUriWithPath("/v1/payments/");
    }

    @Override
    public URI getExistingResourceUri() {
        throw new UnsupportedOperationException(
                "getExistingResourceUri is not to be used in real tests - create the resource instead");
        // TODO remove from interface once no longer used
    }

    /**
     * Build a URI using "localhost", {@link #port}, and the given {@code path}.
     */
    private URI buildUriWithPath(String path) {
        try {
            return new URI(HTTP_SCHEME, null, LOCALHOST, port, path, null, null);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to create collection URI for port " + port, e);
        }
    }

}