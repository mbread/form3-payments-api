/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static net.painfamily.form3.paymentsapi.doctests.matchers.BaseMediaTypeMatches.baseMediaTypeMatches;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.restassured.response.Response;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class CreatePaymentTest extends AbstractDocTest {

    /**
     * The path that can be passed to
     * {@link io.restassured.response.ValidatableResponse#body(String, org.hamcrest.Matcher, Object...)}
     * or {@link io.restassured.path.json.JsonPath#getMap(String)} to retrieve the
     * root object of the JSON.
     */
    private static final String ROOT_JSON_PATH = "";

    private String clientGeneratedId;
    private Response creationResponse;

    @Override
    @Before
    public void setUp() {
        super.setUp();

        clientGeneratedId = "4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43";

        creationResponse =
        given(this.documentationSpec)
            .contentType(MEDIA_TYPE)
            .body("{\r\n"
                + "    \"data\": {\r\n"
                + "		\"type\": \"payments\",\r\n"
                + "		\"id\": \"" + clientGeneratedId + "\",\r\n"
                + "		\"attributes\": {\r\n"
                + "			\"amount\": \"100.21\",\r\n"
                + "			\"beneficiary_party\": {\r\n"
                + "				\"account_name\": \"W Owens\",\r\n"
                + "				\"account_number\": \"31926819\",\r\n"
                + "				\"account_number_code\": \"BBAN\",\r\n"
                + "				\"account_type\": 0,\r\n"
                + "				\"address\": \"1 The Beneficiary Localtown SE2\",\r\n"
                + "				\"bank_id\": \"403000\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\",\r\n"
                + "				\"name\": \"Wilfred Jeremiah Owens\"\r\n"
                + "			},\r\n"
                + "			\"charges_information\": {\r\n"
                + "				\"bearer_code\": \"SHAR\",\r\n"
                + "				\"sender_charges\": [{\r\n"
                + "					\"amount\": \"5.00\",\r\n"
                + "					\"currency\": \"GBP\"\r\n"
                + "				}, {\r\n"
                + "					\"amount\": \"10.00\",\r\n"
                + "					\"currency\": \"USD\"\r\n"
                + "				}],\r\n"
                + "				\"receiver_charges_amount\": \"1.00\",\r\n"
                + "				\"receiver_charges_currency\": \"USD\"\r\n"
                + "			},\r\n"
                + "			\"currency\": \"GBP\",\r\n"
                + "			\"debtor_party\": {\r\n"
                + "				\"account_name\": \"EJ Brown Black\",\r\n"
                + "				\"account_number\": \"GB29XABC10161234567801\",\r\n"
                + "				\"account_number_code\": \"IBAN\",\r\n"
                + "				\"address\": \"10 Debtor Crescent Sourcetown NE1\",\r\n"
                + "				\"bank_id\": \"203301\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\",\r\n"
                + "				\"name\": \"Emelia Jane Brown\"\r\n"
                + "			},\r\n"
                + "			\"end_to_end_reference\": \"Wil piano Jan\",\r\n"
                + "			\"fx\": {\r\n"
                + "				\"contract_reference\": \"FX123\",\r\n"
                + "				\"exchange_rate\": \"2.00000\",\r\n"
                + "				\"original_amount\": \"200.42\",\r\n"
                + "				\"original_currency\": \"USD\"\r\n"
                + "			},\r\n"
                + "			\"numeric_reference\": \"1002001\",\r\n"
                + "			\"payment_id\": \"123456789012345678\",\r\n"
                + "			\"payment_purpose\": \"Paying for goods/services\",\r\n"
                + "			\"payment_scheme\": \"FPS\",\r\n"
                + "			\"payment_type\": \"Credit\",\r\n"
                + "			\"processing_date\": \"2017-01-18\",\r\n"
                + "			\"reference\": \"Payment for Em's piano lessons\",\r\n"
                + "			\"scheme_payment_sub_type\": \"InternetBanking\",\r\n"
                + "			\"scheme_payment_type\": \"ImmediatePayment\",\r\n"
                + "			\"sponsor_party\": {\r\n"
                + "				\"account_number\": \"56781234\",\r\n"
                + "				\"bank_id\": \"123123\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\"\r\n"
                + "			}\r\n"
                + "    	}\r\n"
                + "	}\r\n"
                + "}")
            .accept(MEDIA_TYPE)
            .filter(document("payments/create",
                requestFields(
                    subsectionWithPath("data").description("The <<resources-payment-retrieve, Payment resource object>> providing the properties of the resource to be created")
                ),
                responseFields(
                    subsectionWithPath("data").description("The <<resources-payment-retrieve, Payment resource object>> representing the created resource"),
                    fieldWithPath("data.links.self").description("The URL of the created resource")
                )
            ))
        .when()
            .post(urlConfig.getCollectionUri());
    }

    @After
    public void tearDown() {
        when()
            .request("DELETE", creationResponse.<String>path("data.links.self"));
    }

    @Test
    public void createPaymentReturnsCreationResponse() throws Exception {
        creationResponse
        .then()
            .statusCode(is(201))
            .contentType(baseMediaTypeMatches(MEDIA_TYPE))
            .body("data.type", is(RESOURCE_TYPE))
            .body("data.links.self", is(urlConfig.getCollectionUri() + clientGeneratedId));
    }

    /**
     * Defererencing the self link should return the same representation as returned
     * by the creation request.
     */
    @Test
    public void createPaymentResponseMatchesPaymentResourceResponse() throws Exception {
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
        .when()
            .get(creationResponse.<String>path("data.links.self"))
        .then()
            .body(ROOT_JSON_PATH, equalTo(creationResponse.body().jsonPath().getMap(ROOT_JSON_PATH)));
    }

}
