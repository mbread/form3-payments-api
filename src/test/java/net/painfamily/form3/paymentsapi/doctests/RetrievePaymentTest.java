/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.restassured.path.json.JsonPath;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class RetrievePaymentTest extends AbstractDocTest {

    @Test
    public void retrivePayment() throws Exception {
        // Given a payment resource exists
        JsonPath paymentResource = givenPaymentResource(urlConfig);

        // When retrieving that resource's representation
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
            .filter(document("payments/retrieve",
                responseFields(
                    fieldWithPath("data.type").description("The type of the resource: " + RESOURCE_TYPE),
                    fieldWithPath("data.id").description("The ID of the resource, unique among all payments in this API"),
                    fieldWithPath("data.links.self").description("The URL for this payment resource"),
                    fieldWithPath("data.attributes.amount").description("The value of the payment being made"),
                    fieldWithPath("data.attributes.currency").description("The currency of the payment being made"),
                    fieldWithPath("data.attributes.end_to_end_reference").description("The end-to-end reference provided when making the payment"),
                    fieldWithPath("data.attributes.numeric_reference").description("The numerical reference provided when creating the payment in this API"),
                    fieldWithPath("data.attributes.reference").description("The textual reference provided when making the payment"),
                    fieldWithPath("data.attributes.payment_id").description("The payment ID provided when creating the payment in this API"),
                    fieldWithPath("data.attributes.payment_purpose").description("The payment purpose provided when creating the payment in this API"),
                    fieldWithPath("data.attributes.payment_scheme").description("The scheme by which the payment is being made"),
                    fieldWithPath("data.attributes.payment_type").description("The type of the payment, provided when creating the payment in this API"),
                    fieldWithPath("data.attributes.processing_date").description("The date on which the payment is to be processed"),
                    fieldWithPath("data.attributes.scheme_payment_type").description("The scheme payment type provided when creating the payment in this API"),
                    fieldWithPath("data.attributes.scheme_payment_sub_type").description("The scheme payment subtype provided when creating the payment in this API"),
                    subsectionWithPath("data.attributes.beneficiary_party").description("An object describing the beneficiary party. See the response example for the allowed properties."),
                    subsectionWithPath("data.attributes.charges_information").description("An object describing the charges that are incurred by this payment. See the response example for the allowed properties."),
                    subsectionWithPath("data.attributes.debtor_party").description("An object describing the debtor party making the payment. See the response example for the allowed properties."),
                    subsectionWithPath("data.attributes.fx").description("An object describing the Forex (FX) currency exchange for the payment. See the response example for the allowed properties."),
                    subsectionWithPath("data.attributes.sponsor_party").description("An object describing the sponsor party for the payment. See the response example for the allowed properties.")
                )
            ))
        .when()
            .get(paymentResource.getString("data.links.self"))
        .then()
            .statusCode(is(200))
            .body("data.type", is(RESOURCE_TYPE))
            .body("data.links.self", is(paymentResource.getString("data.links.self")));
    }

}
