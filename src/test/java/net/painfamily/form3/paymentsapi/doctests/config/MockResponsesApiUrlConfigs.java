package net.painfamily.form3.paymentsapi.doctests.config;

import java.net.URI;

public final class MockResponsesApiUrlConfigs {
    /**
     * The URL to a collection response. The default mocked return value to
     * {@link ApiUrlConfig#getCollectionUri()}.
     */
    private final static URI PLAIN_COLLECTION_URI = URI.create("http://www.mocky.io/v2/5ba6b2cd3200006100963dc1");

    /**
     * The URL to a Payment resource response. The default mocked return value to
     * {@link ApiUrlConfig#getExistingResourceUri()}.
     */
    private final static URI PLAIN_RESOURCE_URI = URI.create("http://www.mocky.io/v2/5ba165403500006a005bbd77");

    private MockResponsesApiUrlConfigs() {
        // Disallow instantiation
    }

    /**
     * An {@link ApiUrlConfig} that returns a collection URI that returns a mock
     * response body containing the expected response from a creation request
     */
    public static final ApiUrlConfig CREATION_RESPONSE_CONFIG = new ApiUrlConfig() {
        private final URI COLLECTION_URI = URI.create("http://www.mocky.io/v2/5ba55cb13100002c00d4da93");

        @Override
        public URI getCollectionUri() {
            return COLLECTION_URI;
        }

        @Override
        public URI getExistingResourceUri() {
            return PLAIN_RESOURCE_URI;
        }
    };

    /**
     * An {@link ApiUrlConfig} that returns a collection URI and a resource URI that
     * each return a mock response body containing the expected response from a GET
     * request
     */
    public static final ApiUrlConfig RETRIEVE_RESOURCES_CONFIG = new ApiUrlConfig() {

        @Override
        public URI getCollectionUri() {
            return PLAIN_COLLECTION_URI;
        }

        @Override
        public URI getExistingResourceUri() {
            return PLAIN_RESOURCE_URI;
        }
    };

    public static final ApiUrlConfig UPDATE_RESPONSE_CONFIG = new ApiUrlConfig() {
        private final URI RESOURCE_URI = URI.create("http://www.mocky.io/v2/5ba6a2da3200005e00963db5");

        @Override
        public URI getCollectionUri() {
            return PLAIN_COLLECTION_URI;
        }

        @Override
        public URI getExistingResourceUri() {
            return RESOURCE_URI;
        }
    };

    public static final ApiUrlConfig DELETION_RESPONSE_CONFIG = new ApiUrlConfig() {
        private final URI RESOURCE_URI = URI.create("http://www.mocky.io/v2/5ba6ac843200002b00963db8");

        @Override
        public URI getCollectionUri() {
            return PLAIN_COLLECTION_URI;
        }

        @Override
        public URI getExistingResourceUri() {
            return RESOURCE_URI;
        }
    };
}
