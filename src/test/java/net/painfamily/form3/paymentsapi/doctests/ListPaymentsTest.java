/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.Test;

import net.painfamily.form3.paymentsapi.doctests.config.ApiUrlConfig;
import net.painfamily.form3.paymentsapi.doctests.config.MockResponsesApiUrlConfigs;

public class ListPaymentsTest extends AbstractDocTest {

    // TODO Replace with a non-mocked config when real implementation is available
    private final ApiUrlConfig urlConfig = MockResponsesApiUrlConfigs.RETRIEVE_RESOURCES_CONFIG;

    @Test
    public void listPayments() throws Exception {
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
            .filter(document("payments/list",
                responseFields(
                    fieldWithPath("data[].type").description("The type of each resource: " + RESOURCE_TYPE),
                    fieldWithPath("data[].id").description("The ID of each resource, unique among all payments in this API"),
                    fieldWithPath("data[].version").description("The version of each resource as defined in the excercise definition sample data"),
                    fieldWithPath("data[].organisation_id").description("The organisation_id value for each payment resource, as defined in the excercise definition sample data"),
                    subsectionWithPath("data[].attributes").description("The attribute values of each resource. See the <<resources-payment-retrieve_response_fields, 'retrieve Payment resource' response>> for the available fields."),
                    fieldWithPath("data[].links.self").description("The URL for each payment resource"),
                    fieldWithPath("links.self").description("The URL of this page of this collection"),
                    fieldWithPath("links.first").description("The URL of the first page of this collection"),
                    fieldWithPath("links.last").description("The URL of the final page of this collection"),
                    fieldWithPath("links.prev").description("The URL of the previous page of this collection")
                        .optional().type("String"),
                    fieldWithPath("links.next").description("The URL of the next page of this collection")
                )
            ))
        .when()
            .get(urlConfig.getCollectionUri())
        .then()
            .statusCode(is(200));
            // TODO: .body("links.self", is(urlConfig.getCollectionUri()))
    }

}
