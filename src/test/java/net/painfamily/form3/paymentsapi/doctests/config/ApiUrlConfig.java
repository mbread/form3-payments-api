package net.painfamily.form3.paymentsapi.doctests.config;

import java.net.URI;

/**
 * An interface defining a provider of URIs to use to access the system under
 * test during testing.
 */
public interface ApiUrlConfig {
    /**
     * @return The URI to use to send a request to the Payments collection or create
     *         a Payment resource
     */
    URI getCollectionUri();

    /**
     * @return The URI to use to retrieve or act on an existing Payment resource
     */
    URI getExistingResourceUri();
}