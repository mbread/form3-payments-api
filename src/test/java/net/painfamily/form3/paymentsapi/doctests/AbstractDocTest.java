package net.painfamily.form3.paymentsapi.doctests;

import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;
import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.restdocs.JUnitRestDocumentation;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.painfamily.form3.paymentsapi.doctests.config.ApiUrlConfig;
import net.painfamily.form3.paymentsapi.doctests.config.LocalhostApiUrlConfig;

public abstract class AbstractDocTest {

    public static final String MEDIA_TYPE = "application/vnd.api+json";
    public static final String RESOURCE_TYPE = "payments";

    @Rule
    public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @LocalServerPort
    protected Integer port;

    protected ApiUrlConfig urlConfig;
    protected RequestSpecification documentationSpec;

    private List<String> resourcesToCleanUp;

    @Before
    public void setUp() {
        resourcesToCleanUp = new ArrayList<>();

        if(port != null) {
            urlConfig = new LocalhostApiUrlConfig(port);
        }

        this.documentationSpec = new RequestSpecBuilder()
            .setPort(port != null ? port : 80)
            .addFilter(
                documentationConfiguration(restDocumentation)
                .operationPreprocessors()
                .withRequestDefaults(
                    modifyUris()
                        .scheme("https")
                        .host("api.example.com")
                        .removePort(),
                    prettyPrint()
                )
                .withResponseDefaults(
                    modifyUris()
                        .scheme("https")
                        .host("api.example.com")
                        .removePort(),
                    prettyPrint()
                )
            ).build();
    }

    @After
    public void tearDown() {
        resourcesToCleanUp.forEach(url -> {
            Response response = given().when().delete(url)
                .then().extract().response();
            int statusCode = response.getStatusCode();
            if (statusCode >= 400) {
                System.err.println("Could not clean up resource: " + url + " Status code: " + statusCode);
            }
        });
    }

    /**
     * @param urlConfig The URL config to use when creating or constructing this
     *                  Payment resource
     * @return A JSON-API response containing the type, ID and self link for a
     *         Payment recource that already exists. This can be used as a
     *         precondition by tests, i.e. by test cases that could start with
     *         "Given a payment resource..."
     */
    protected JsonPath givenPaymentResource(ApiUrlConfig urlConfig) {
        Response response = given(this.documentationSpec)
            .contentType(MEDIA_TYPE)
            // TODO extract to resource file (at least "attributes" object)
            .body("{\r\n"
                + "    \"data\": {\r\n"
                + "		\"type\": \"payments\",\r\n"
                + "		\"id\": \"" + UUID.randomUUID() + "\",\r\n"
                + "		\"attributes\": {\r\n"
                + "			\"amount\": \"100.21\",\r\n"
                + "			\"beneficiary_party\": {\r\n"
                + "				\"account_name\": \"W Owens\",\r\n"
                + "				\"account_number\": \"31926819\",\r\n"
                + "				\"account_number_code\": \"BBAN\",\r\n"
                + "				\"account_type\": 0,\r\n"
                + "				\"address\": \"1 The Beneficiary Localtown SE2\",\r\n"
                + "				\"bank_id\": \"403000\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\",\r\n"
                + "				\"name\": \"Wilfred Jeremiah Owens\"\r\n"
                + "			},\r\n"
                + "			\"charges_information\": {\r\n"
                + "				\"bearer_code\": \"SHAR\",\r\n"
                + "				\"sender_charges\": [{\r\n"
                + "					\"amount\": \"5.00\",\r\n"
                + "					\"currency\": \"GBP\"\r\n"
                + "				}, {\r\n"
                + "					\"amount\": \"10.00\",\r\n"
                + "					\"currency\": \"USD\"\r\n"
                + "				}],\r\n"
                + "				\"receiver_charges_amount\": \"1.00\",\r\n"
                + "				\"receiver_charges_currency\": \"USD\"\r\n"
                + "			},\r\n"
                + "			\"currency\": \"GBP\",\r\n"
                + "			\"debtor_party\": {\r\n"
                + "				\"account_name\": \"EJ Brown Black\",\r\n"
                + "				\"account_number\": \"GB29XABC10161234567801\",\r\n"
                + "				\"account_number_code\": \"IBAN\",\r\n"
                + "				\"address\": \"10 Debtor Crescent Sourcetown NE1\",\r\n"
                + "				\"bank_id\": \"203301\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\",\r\n"
                + "				\"name\": \"Emelia Jane Brown\"\r\n"
                + "			},\r\n"
                + "			\"end_to_end_reference\": \"Wil piano Jan\",\r\n"
                + "			\"fx\": {\r\n"
                + "				\"contract_reference\": \"FX123\",\r\n"
                + "				\"exchange_rate\": \"2.00000\",\r\n"
                + "				\"original_amount\": \"200.42\",\r\n"
                + "				\"original_currency\": \"USD\"\r\n"
                + "			},\r\n"
                + "			\"numeric_reference\": \"1002001\",\r\n"
                + "			\"payment_id\": \"123456789012345678\",\r\n"
                + "			\"payment_purpose\": \"Paying for goods/services\",\r\n"
                + "			\"payment_scheme\": \"FPS\",\r\n"
                + "			\"payment_type\": \"Credit\",\r\n"
                + "			\"processing_date\": \"2017-01-18\",\r\n"
                + "			\"reference\": \"Payment for Em's piano lessons\",\r\n"
                + "			\"scheme_payment_sub_type\": \"InternetBanking\",\r\n"
                + "			\"scheme_payment_type\": \"ImmediatePayment\",\r\n"
                + "			\"sponsor_party\": {\r\n"
                + "				\"account_number\": \"56781234\",\r\n"
                + "				\"bank_id\": \"123123\",\r\n"
                + "				\"bank_id_code\": \"GBDSC\"\r\n"
                + "			}\r\n"
                + "    	}\r\n"
                + "	}\r\n"
                + "}")
            .accept(MEDIA_TYPE)
        .when()
            .post(urlConfig.getCollectionUri())
        .then().extract().response();

        JsonPath jsonPath = response.jsonPath();
        resourcesToCleanUp.add(jsonPath.getString("data.links.self"));

        return jsonPath;
    }
}
