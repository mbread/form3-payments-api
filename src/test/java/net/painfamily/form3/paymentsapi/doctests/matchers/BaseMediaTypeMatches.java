package net.painfamily.form3.paymentsapi.doctests.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class BaseMediaTypeMatches extends TypeSafeMatcher<String> {

    private String mediaType;

    public BaseMediaTypeMatches(String mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    protected boolean matchesSafely(String item) {
        if(item == null) return false;
        return mediaType.equals(item) || item.startsWith(mediaType + ";");
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("base media type matches");
        description.appendValue(mediaType);
    }

    public static BaseMediaTypeMatches baseMediaTypeMatches(String mediaType) {
        return new BaseMediaTypeMatches(mediaType);
    }

}