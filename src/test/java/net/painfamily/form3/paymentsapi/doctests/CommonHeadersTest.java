/*
 * This class is a based on a work that has the following copyright and licence notice:
 *
 * Copyright 2014-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.painfamily.form3.paymentsapi.doctests;

import static io.restassured.RestAssured.given;
import static net.painfamily.form3.paymentsapi.doctests.matchers.BaseMediaTypeMatches.baseMediaTypeMatches;
import static org.hamcrest.CoreMatchers.*;
import static org.springframework.restdocs.headers.HeaderDocumentation.*;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
public class CommonHeadersTest extends AbstractDocTest {

    @Test
    public void commonHeaders() throws Exception {
        given(this.documentationSpec)
            .accept(MEDIA_TYPE)
            .filter(document("common-headers",
                requestHeaders(
                    headerWithName("Accept").description("JSON-API media type: " + MEDIA_TYPE)
                ),
                responseHeaders(
                    headerWithName("Content-Type").description("JSON-API media type: " + MEDIA_TYPE + " (optionally with charset parameter)")
                )
            ))
        .when()
            .get(urlConfig.getCollectionUri())
        .then()
            .statusCode(is(200))
            .contentType(baseMediaTypeMatches(MEDIA_TYPE));
    }

}
