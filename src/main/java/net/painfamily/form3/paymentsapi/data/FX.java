package net.painfamily.form3.paymentsapi.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An object describing the Forex (FX) currency exchange for a {@link Payment}.
 */
public class FX {
    
    @JsonProperty("contract_reference")
    private String contractReference;
    
    @JsonProperty("exchange_rate")
    private String exchangeRate;
    
    @JsonProperty("original_amount")
    private String originalAmount;
    
    @JsonProperty("original_currency")
    private String originalCurrency;

    public String contractReference() {
        return contractReference;
    }

    public void contractReference(String value) {
        this.contractReference = value;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String value) {
        this.exchangeRate = value;
    }

    public String getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(String value) {
        this.originalAmount = value;
    }

    public String getOriginalCurrency() {
        return originalCurrency;
    }

    public void setOriginalCurrency(String value) {
        this.originalCurrency = value;
    }
}