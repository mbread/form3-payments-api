package net.painfamily.form3.paymentsapi.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An object describing the charges that are incurred by a {@link Payment}.
 */
public class ChargesInformation {
    @JsonProperty("bearer_code")
    private String bearerCode;

    @JsonProperty("sender_charges")
    private List<Charge> senderCharges;
    
    @JsonProperty("receiver_charges_amount")
    private String receiverChargesAmount;
    
    @JsonProperty("receiver_charges_currency")
    private String receiverChargesCurrency;

    public String getBearerCode() {
        return bearerCode;
    }

    public void setBearerCode(String value) {
        this.bearerCode = value;
    }

    public List<Charge> senderCharges() {
        return senderCharges;
    }

    public void senderCharges(List<Charge> value) {
        this.senderCharges = value;
    }

    public String getReceiverChargesAmount() {
        return receiverChargesAmount;
    }

    public void setReceiverChargesAmount(String value) {
        this.receiverChargesAmount = value;
    }

    public String getReceiverChargesCurrency() {
        return receiverChargesCurrency;
    }

    public void setReceiverChargesCurrency(String value) {
        this.receiverChargesCurrency = value;
    }
}