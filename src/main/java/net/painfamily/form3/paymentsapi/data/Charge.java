package net.painfamily.form3.paymentsapi.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * One of a number of charges in {@link ChargesInformation} for a
 * {@link Payment}.
 */
public class Charge {
    @JsonProperty("amount")
    private String amount;
    
    @JsonProperty("currency")
    private String currency;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String value) {
        this.amount = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String value) {
        this.currency = value;
    }
}