package net.painfamily.form3.paymentsapi.data;

import java.time.LocalDate;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiResource;

/**
 * This class is responsible for holding the data related to a payment, and is
 * annotated for serialising and deserialising that data over this service's
 * JSON-API interface.
 */
@JsonApiResource(type = "payments")
public class Payment {

    @JsonApiId
    private UUID id;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("end_to_end_reference")
    private String endToEndReference;

    @JsonProperty("numeric_reference")
    private String numericReference;

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("payment_id")
    private String paymentId;

    @JsonProperty("payment_purpose")
    private String paymentPurpose;

    @JsonProperty("payment_scheme")
    private String paymentScheme;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("processing_date")
    private LocalDate processingDate;

    @JsonProperty("scheme_payment_type")
    private String schemePaymentType;

    @JsonProperty("scheme_payment_sub_type")
    private String schemePaymentSubType;

    @JsonProperty("beneficiary_party")
    private Party beneficiaryParty;

    @JsonProperty("sponsor_party")
    private Party sponsorParty;

    @JsonProperty("debtor_party")
    private Party debtorParty;

    @JsonProperty("charges_information")
    private ChargesInformation chargesInformation;

    @JsonProperty("fx")
    private FX fx;

    public Payment() {
    }

    public Payment(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String value) {
        this.amount = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String value) {
        this.currency = value;
    }

    public String getEndToEndReference() {
        return endToEndReference;
    }

    public void setEndToEndReference(String value) {
        this.endToEndReference = value;
    }

    public String getNumericReference() {
        return numericReference;
    }

    public void setNumericReference(String value) {
        this.numericReference = value;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String value) {
        this.reference = value;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String value) {
        this.paymentId = value;
    }

    public String getPaymentPurpose() {
        return paymentPurpose;
    }

    public void setPaymentPurpose(String value) {
        this.paymentPurpose = value;
    }

    public String getPaymentScheme() {
        return paymentScheme;
    }

    public void setPaymentScheme(String value) {
        this.paymentScheme = value;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    public LocalDate getProcessingDate() {
        return processingDate;
    }

    public void setProcessingDate(LocalDate value) {
        this.processingDate = value;
    }

    public String getSchemePaymentType() {
        return schemePaymentType;
    }

    public void setSchemePaymentType(String value) {
        this.schemePaymentType = value;
    }

    public String getSchemePaymentSubType() {
        return schemePaymentSubType;
    }

    public void setSchemePaymentSubType(String value) {
        this.schemePaymentSubType = value;
    }

    public Party getBeneficiaryParty() {
        return beneficiaryParty;
    }

    public void setBeneficiaryParty(Party value) {
        this.beneficiaryParty = value;
    }

    public Party getSponsorParty() {
        return sponsorParty;
    }

    public void setSponsorParty(Party value) {
        this.sponsorParty = value;
    }

    public Party getDebtorParty() {
        return debtorParty;
    }

    public void setDebtorParty(Party value) {
        this.debtorParty = value;
    }

    public ChargesInformation getChargesInformation() {
        return chargesInformation;
    }

    public void setChargesInformation(ChargesInformation value) {
        this.chargesInformation = value;
    }

    public FX getFx() {
        return fx;
    }

    public void setFx(FX value) {
        this.fx = value;
    }
}