package net.painfamily.form3.paymentsapi.data;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An object describing the beneficiary party, sponsor party or debtor party of
 * a {@link Payment}. Specifically, it describes that party in terms of their
 * bank account that is involved in the payment.
 */
public class Party {
    @JsonProperty("account_name")
    private String accountName;
    
    @JsonProperty("account_number")
    private String accountNumber;
    
    @JsonProperty("account_number_code")
    private String accountNumberCode;
    
    @JsonProperty("account_type")
    private int accountType;
    
    @JsonProperty("address")
    private String address;
    
    @JsonProperty("bank_id")
    private String bankId;
    
    @JsonProperty("bank_id_code")
    private String bankIdCode;
    
    @JsonProperty("name")
    private String name;
    
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String value) {
        this.accountName = value;
    }
    
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }
    
    public String getAccountNumberCode() {
        return accountNumberCode;
    }

    public void setAccountNumberCode(String value) {
        this.accountNumberCode = value;
    }
    
    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int value) {
        this.accountType = value;
    }
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String value) {
        this.address = value;
    }
    
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String value) {
        this.bankId = value;
    }
    
    public String getBankIdCode() {
        return bankIdCode;
    }

    public void setBankIdCode(String value) {
        this.bankIdCode = value;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }
}