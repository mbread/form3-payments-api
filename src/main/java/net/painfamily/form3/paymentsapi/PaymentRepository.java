package net.painfamily.form3.paymentsapi;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import net.painfamily.form3.paymentsapi.data.Payment;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class PaymentRepository extends ResourceRepositoryBase<Payment, UUID> {

    private Map<UUID, Payment> projects = new HashMap<>();

    public PaymentRepository() {
        super(Payment.class);
    }

    @Override
    public synchronized void delete(UUID id) {
        projects.remove(id);
    }

    @Override
    public synchronized <S extends Payment> S save(S project) {
        if (project.getId() == null) {
            project.setId(UUID.randomUUID());
        }
        projects.put(project.getId(), project);
        return project;
    }

    @Override
    public synchronized ResourceList<Payment> findAll(QuerySpec querySpec) {
        return querySpec.apply(projects.values());
    }

}