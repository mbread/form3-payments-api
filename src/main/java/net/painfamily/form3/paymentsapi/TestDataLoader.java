package net.painfamily.form3.paymentsapi;

import net.painfamily.form3.paymentsapi.data.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

import javax.annotation.PostConstruct;

@Configuration
public class TestDataLoader {

    @Autowired
    private PaymentRepository projectRepository;

    @PostConstruct
    public void setup() {
        projectRepository.save(new Payment(UUID.randomUUID()));
        projectRepository.save(new Payment(UUID.randomUUID()));
        projectRepository.save(new Payment(UUID.randomUUID()));
        projectRepository.save(new Payment(UUID.randomUUID()));
    }
}