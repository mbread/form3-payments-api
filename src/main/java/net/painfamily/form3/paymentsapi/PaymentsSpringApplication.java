package net.painfamily.form3.paymentsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@SpringBootApplication(scanBasePackages="net.painfamily.form3.paymentsapi")
@Import({ TestDataLoader.class })
public class PaymentsSpringApplication {

    public static void main(String[] args) {
        new SpringApplication(PaymentsSpringApplication.class).run(args);
    }

}
